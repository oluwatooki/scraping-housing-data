# Housing Data Scraping Project

## Overview
This project involves web scraping housing data from Zillow to gather information about rental listings. The collected data includes property addresses, rental prices, and direct links to the listings. The aim is to create a comprehensive dataset that can be further analyzed for insights into the rental market.

## Project Components

### Web Scraping
The web scraping process is implemented using Python with the BeautifulSoup library. The script navigates through the Zillow website, extracts relevant information from the HTML, and organizes it into usable data.

### Data Submission
The collected data is submitted using a Google Form. This step involves automating the form submission process for each housing listing, ensuring a streamlined and efficient approach.

## Tools Used
- Python
- BeautifulSoup
- Google Forms
- Selenium (for form submission)

## Execution
1. Run the web scraping script to collect housing data from Zillow.
2. Execute the data submission script to populate a Google Form with the gathered information.

## Note
Ensure you have the necessary dependencies installed before running the scripts.

Feel free to explore, modify, and adapt the code according to your requirements. If you encounter any issues or have suggestions for improvement, please provide feedback.
