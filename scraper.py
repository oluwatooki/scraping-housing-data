# Import necessary libraries
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

# Set user agent for header to avoid blocking
header = {
    "Your headers":"Your headers"
}

# URL for Zillow's rental listings and Google Form
zillow_url = 'https://www.zillow.com/homes/for_rent/?searchQueryState=%7B"pagination"%3A%7B%7D%2C"mapBounds"%3A%7B"west"%3A' \
             '-122.7303031821289%2C"east"%3A-122.1363548178711%2C"south"%3A37.656879965418064%2C"north"%3A37.89351359163887' \
             '%7D%2C"mapZoom"%3A11%2C"isMapVisible"%3Atrue%2C"filterState"%3A%7B"price"%3A%7B"max"%3A872627%7D%2C"beds"%3A' \
             '%7B"min"%3A1%7D%2C"fore"%3A%7B"value"%3Afalse%7D%2C"mp"%3A%7B"max"%3A3000%7D%2C"auc"%3A%7B"value"%3Afalse' \
             '%7D%2C"nc"%3A%7B"value"%3Afalse%7D%2C"fr"%3A%7B"value"%3Atrue%7D%2C"fsbo"%3A%7B"value"%3Afalse%7D%2C"cmsn"%3A' \
             '%7B"value"%3Afalse%7D%2C"fsba"%3A%7B"value"%3Afalse%7D%7D%2C"isListVisible"%3Atrue%7D'
google_form_url = 'Your Form Link'

# Send request to Zillow and get HTML content
content = requests.get(url=zillow_url, headers=header).text

# Function to format links
def link_formatter(list_link_input) -> list:
    links = []
    count = 0
    for i in list_link_input:
        raw_link = str(i.get('href'))
        if count == 9:
            break
        if not raw_link.startswith('https://'):
            link = f"https://www.zillow.com{raw_link}"
        else:
            link = raw_link
        count += 1
        links.append(link)
    return links

# Use BeautifulSoup for parsing HTML content
soup = BeautifulSoup(content, 'html.parser')
linking_list = soup.find_all(name='a', class_='property-card-link')
price_links = soup.select('div.StyledPropertyCardDataArea-c11n-8-85-1__sc-yipmu-0 span')
address_links = soup.find_all(name='address')

# Extract data from Zillow
price_list = [str(price.text).replace('+', '').replace('/', '').replace('mo', '').split()[0] for price in price_links]
address_list = [str(address.text) for address in address_links]
link_list = link_formatter(linking_list)

# Display extracted data
print(price_list)
print(address_list)
print(link_list)

# Set up Selenium for form submission
options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)
chrome_drive_path = "path"
driver = webdriver.Chrome(service=chrome_drive_path, options=options)
driver.implicitly_wait(6)
driver.get(google_form_url)

# Dictionary to store information
info_dict = {
    1: address_list,
    2: price_list,
    3: link_list
}

# Loop through links and submit data to Google Form
for i in range(len(link_list)):
    for x in range(1, 4):
        info = info_dict[x]
        # clicks on the items
        driver.find_element(by=By.XPATH,
                            value=f'//*[@id="mG61Hd"]/div[2]/div/div[2]/div[{x}]/div/div/div[2]/div/div[1]/div/div['
                                  f'1]/input').click()
        time.sleep(1)
        # sends the info
        driver.find_element(by=By.XPATH,
                            value=f'//*[@id="mG61Hd"]/div[2]/div/div[2]/div[{x}]/div/div/div[2]/div/div[1]/div/div['
                                  f'1]/input').send_keys(info[i])
    # clicks on submit
    driver.find_element(by=By.XPATH, value='//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div[1]/div/span/span').click()
    time.sleep(1)
    print('success')
    # clicks on enter new for response
    driver.find_element(by=By.XPATH, value='/html/body/div[1]/div[2]/div[1]/div/div[4]/a').click()